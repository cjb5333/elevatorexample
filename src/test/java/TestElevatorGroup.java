import models.Elevator;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

public class TestElevatorGroup {
    private ElevatorGroup elevatorGroup;

    @Before
    public void setUp() {
        elevatorGroup = new ElevatorGroup();
    }

    @Test
    public void shouldMoveElevator() {
        elevatorGroup.moveElevator(3, 5);
        Elevator elevator = elevatorGroup.getElevator(3);
        assertEquals(elevator.getCurrentFloor(), 5);
    }

    @Test
    public void shouldNotGetElevator() {
        elevatorGroup.getElevator(10);
    }

    @Test
    public void shouldGetClosestElevator() {
        int elevatorIndexForTest = 3;
        elevatorGroup.moveElevator(elevatorIndexForTest, 5);
        Elevator closestElevator = elevatorGroup.getElevatorClosestToFloor(4);
        assertEquals(closestElevator, elevatorGroup.getElevator(elevatorIndexForTest));
    }

    @Test
    public void shouldNotMoveElevator() {
        elevatorGroup.moveElevator(5, 10);
        // TODO for Benet - create an exception and throw
        fail("TODO for Benet");
    }
}
