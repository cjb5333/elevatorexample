import exceptions.InvalidFloorException;
import models.Elevator;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

public class TestElevator {
    private Elevator elevator;

    @Before
    public void setUp() {
        elevator = new Elevator();
    }

    @Test
    public void shouldStartAtGroundFloor() {
        assertEquals(1, elevator.getCurrentFloor());
    }

    @Test
    public void shouldGoToSecondFloor() throws InvalidFloorException {
        elevator.goToFloor(2);
        assertEquals(2, elevator.getCurrentFloor());
    }

    @Test
    public void shouldMoveUpTwoFloors() throws InvalidFloorException {
        elevator.goToFloor(1);
        assertEquals(1, elevator.getCurrentFloor());
        elevator.move(2);
        assertEquals(3, elevator.getCurrentFloor());
    }

    @Test
    public void shouldNotExceedMaxFloor() {
        try {
            elevator.setMaxFloor(10);
            elevator.goToFloor(12);
            fail("InvalidFloorException was not thrown");
        } catch(InvalidFloorException exception) {
            assertEquals(exception.getMessage(), "An invalid floor has been entered");
        }
    }

    @Test
    public void shouldSetMaxFloor() {
        // TODO for Benet
        fail("TODO for Benet");
    }

    @Test
    public void shouldGoToSecondThenThirdFloor() {
        // TODO for Benet
        fail("TODO for Benet");
    }

    @Test
    public void shouldNotMovePastMaxFloor() {
        // TODO for Benet
        // set max floor, attempt to move past that, throw an exception if it exceeds that
        fail("TODO for Benet");
    }
}
