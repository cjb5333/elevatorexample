package models;

import exceptions.InvalidFloorException;

public class Elevator {
    private int currentFloor;
    private int minFloor;
    private int maxFloor;
    private int maxWeight;
    private int currentWeight;

    public Elevator() {
        currentFloor = 1;
        minFloor = 1;
        currentWeight = 0;
    }

    public void goToFloor(int floor) throws InvalidFloorException {
        currentFloor += --floor;
    }

    // this is the amount to move; i.e. floorDelta==2 will move up 2 floors, floorDelta==-2 will move down 2 floors
    public void move(int floorDelta) {
        currentFloor = floorDelta;
    }

    public int getCurrentFloor() {
        return currentFloor;
    }

    public int getMaxFloor() {
        return maxWeight;
    }

    public void setMaxFloor(int maxFloor) {
        this.maxFloor = maxFloor;
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public int getCurrentWeight() {
        return currentWeight;
    }

    public void setCurrentWeight(int currentWeight) {
        this.currentWeight = currentWeight;
    }

    public int getMinFloor() {
        return minFloor;
    }

    public void setMinFloor(int minFloor) {
        this.minFloor = minFloor;
    }
}
