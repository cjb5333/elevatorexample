import models.Elevator;

public class ElevatorGroup {
    private Elevator[] elevators;

    public ElevatorGroup() {
        elevators = new Elevator[4];
        elevators[0] = new Elevator();
        elevators[1] = new Elevator();
        elevators[2] = new Elevator();
        elevators[3] = new Elevator();
    }

    public void sendElevatorToFloor(int floor) {
        // TODO for Benet
    }

    public Elevator getElevatorClosestToFloor(int floor) {
        Elevator closestElevator = elevators[0];
        for(int i=1; i<4; i++) {
            if(elevators[i].getCurrentFloor()-floor < closestElevator.getCurrentFloor()-floor)
                closestElevator = elevators[i];
        }
        return closestElevator;
    }

    public void moveElevator(int elevatorIndex, int floor) {
        elevators[elevatorIndex].move(floor);
    }

    public Elevator getElevator(int index) {
        return elevators[index];
    }
}
