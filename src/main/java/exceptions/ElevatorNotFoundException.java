package exceptions;

public class ElevatorNotFoundException extends Exception {
    private static final String ELEVATOR_NOT_FOUND_MESSAGE = "Elevator not found.";

    public ElevatorNotFoundException() {
        super(ELEVATOR_NOT_FOUND_MESSAGE);
    }
}
