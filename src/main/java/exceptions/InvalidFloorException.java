package exceptions;

public class InvalidFloorException extends Exception {
    private static final String INVALID_FLOOR_MESSAGE = "An invalid floor has been entered";

    public InvalidFloorException() {
        super(INVALID_FLOOR_MESSAGE);
    }
}
